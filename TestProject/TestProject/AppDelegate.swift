//
//  AppDelegate.swift
//  TestProject
//
//  Created by Vishnu Anilkumar on 06/08/18.
//  Copyright © 2018 Vishnu Anilkumar. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

